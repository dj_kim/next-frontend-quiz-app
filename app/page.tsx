import Link from "next/link";
import Card from "./components/Card";
import data from "@/app/lib/data.json";

export default function Home() {
  return (
    <div className="pt-8 sm:pt-16 lg:flex justify-between gap-32">
      <div>
        <div className="flex flex-col items-start gap-2 text-dark-navy dark:text-white text-[40px] sm:text-[64px] leading-none">
          <p className="font-light">Welcome to the</p>
          <span className="font-medium">Frontend Quiz!</span>
        </div>
        <p className="italic text-md text-grey-navy dark:text-light-bluish mt-4 lg:mt-12">
          Pick a subject to get started.
        </p>
      </div>

      <ul className="mt-10 sm:mt-16 lg:mt-0 flex-1">
        {data.quizzes.map((item, index) => {
          return (
            <li className={`mb-3 lg:mb-6`} key={`${index}-${item.title}`}>
              <Link href={`/quiz/${item.title.toLowerCase()}`}>
                <Card icon={item.icon} title={item.title} />
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
