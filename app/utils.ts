import data from "@/app/lib/data.json";

export const colorVariants = {
  HTML: 'bg-light-orange',
  CSS: 'bg-light-green',
  JavaScript: 'bg-light-blue',
  Accessibility: 'bg-light-purple'
}

export function findQuizBySlug(slug: string) {
  for (const quiz of data.quizzes) {
    if (quiz.title.toLocaleLowerCase() === slug) {
      return quiz
    }
  }
}

export function setOptionCard(option: string, selectedValue: string, submitted: boolean, isCorrect: boolean, answer: string) {
  let outline = ""
  let icon = ""
  let color = ""

  if (option === selectedValue && !submitted) {
    outline = "outline-primary"
    color = "bg-primary"
    icon = ""
  } else if (submitted && option === answer) {
    if (option === selectedValue) {
      outline = "outline-secondary"
      color = "bg-secondary"
      icon = "correct"
    } else {
      icon = "correct"
    }
  } else if (submitted && option === selectedValue && !isCorrect) {
    outline = "outline-red-500"
    color = "bg-red-500"
    icon = "error"
  }

  return { outline, color, icon }
}