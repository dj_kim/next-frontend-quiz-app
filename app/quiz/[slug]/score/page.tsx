"use client";

import { useContext } from "react";
import { SlugScoreContext } from "@/app/SlugScoreProvider";
import TopicCard from "@/app/components/TopicCard";
import Link from "next/link";

export default function Page() {
  const { totalScore } = useContext(SlugScoreContext);

  return (
    <div className="sm:h-full lg:flex lg:gap-[143px] py-8 sm:py-10 lg:mt-[85px]">
      <div className="flex flex-col items-start gap-2 text-dark-navy dark:text-white text-[40px] sm:text-[64px] leading-none mb-10 sm:mb-16 lg:min-w-[450px]">
        <p className="font-light">Quiz completed</p>
        <span className="font-medium">You scored...</span>
      </div>
      <div className="flex flex-col justify-center w-full">
        <div className="flex flex-col items-center justify-center w-full bg-white dark:bg-navy p-8 sm:p-12 rounded-xl sm:rounded-3xl h-min">
          <TopicCard />
          <p className="text-[88px] sm:text-[144px] lg:text-[188px] leading-none font-medium text-dark-navy dark:text-white mt-4 sm:mt-10">
            {totalScore}
          </p>
          <p className="text-lg sm:text-2xl text-grey-navy dark:text-light-bluish mt-4">
            out of 10
          </p>
        </div>
        <Link
          href="/"
          className="mt-3 sm:mt-8 select-none w-full bg-primary text-white text-lg sm:text-[28px] py-3 px-8 sm:p-8 rounded-xl sm:rounded-3xl font-medium text-center"
        >
          Play Again
        </Link>
      </div>
    </div>
  );
}
