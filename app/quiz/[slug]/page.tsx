"use client";

import { useContext, useEffect, useState } from "react";
import data from "@/app/lib/data.json";
import ProgressBar from "@/app/components/ProgressBar";
import { useRouter } from "next/navigation";
import OptionCard from "@/app/components/OptionCard";
import Image from "next/image";
import { useMediaQuery } from "react-responsive";
import { SlugScoreContext } from "@/app/SlugScoreProvider";
import { findQuizBySlug, setOptionCard } from "@/app/utils";

export default function Page({ params }: { params: { slug: string } }) {
  const router = useRouter();
  const isMobile = useMediaQuery({ query: "(max-width: 640px)" });
  const { setTotalScore } = useContext(SlugScoreContext);

  const [quiz, setQuiz] = useState<{
    title: string;
    icon: string;
    questions: { question: string; options: string[]; answer: string }[];
  }>({
    title: "",
    icon: "",
    questions: [{ question: "", options: [], answer: "" }],
  });

  const [score, setScore] = useState(0);
  const [progress, setProgress] = useState(0);
  const [selectedValue, setSelectedValue] = useState("");
  const [submitted, setSubmitted] = useState(false);
  const [isCorrect, setIsCorrect] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isError, setIsError] = useState(false);

  let iconSize = isMobile ? 32 : 40;

  function handleButtonClick(option: string) {
    if (!submitted) {
      setSelectedValue(option);
    }
  }

  function checkAnswer() {
    if (selectedValue === quiz.questions[progress].answer) {
      setScore((prev) => prev + 1);
      setIsCorrect(true);
    } else {
      setIsCorrect(false);
    }
    setIsError(false);
    setSubmitted(true);
  }

  function handleSubmit(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    e.preventDefault();
    if (selectedValue === "") {
      setIsError(true);
      return;
    }
    if (submitted) {
      if (progress === 9) {
        setTotalScore(score);
        router.push(`/quiz/${params.slug}/score`);
      } else {
        setProgress((prev) => prev + 1);
        setSubmitted(false);
        setIsCorrect(false);
        setSelectedValue("");
      }
      return;
    }
    checkAnswer();
  }

  useEffect(() => {
    const quiz = findQuizBySlug(params.slug);
    if (quiz) {
      setQuiz(quiz);
    }
  }, []);

  useEffect(() => {
    if (quiz.title !== "") {
      setIsLoaded(true);
    }
  }, [quiz]);

  return !isLoaded ? (
    <div className="h-screen">Loading...</div>
  ) : (
    <div className="py-8 lg:flex gap-[130px]">
      <div className="basis-1/2">
        <p className="text-sm dark:text-light-bluish italic leading-6 lg:text-[20px]">
          Question {progress + 1} of 10
        </p>
        <p className="mt-3 dark:text-white text-[20px] font-medium leading-5 sm:text-4xl lg:mt-7 text-left">
          {quiz.questions[progress].question}
        </p>
        <div className="mt-6 lg:mt-40">
          <ProgressBar progress={progress + 1} total={10} />
        </div>
      </div>
      <div className="basis-1/2">
        <ul className="mt-10 dark:text-white">
          {quiz.questions[progress].options?.map((option, index) => {
            const settings = setOptionCard(
              option,
              selectedValue,
              submitted,
              isCorrect,
              quiz.questions[progress].answer
            );
            return (
              <OptionCard
                key={index}
                index={index}
                option={option}
                outline={settings.outline}
                color={settings.color}
                icon={settings.icon}
                handleButtonClick={handleButtonClick}
              />
            );
          })}
        </ul>
        <div className="py-10">
          <button
            className="select-none w-full bg-primary text-white text-lg sm:text-[28px] py-3 px-8 sm:p-8 rounded-xl sm:rounded-3xl font-medium hover:bg-primary/[0.5]"
            onClick={handleSubmit}
          >
            {submitted ? "Next Question" : "Submit Answer"}
          </button>
          {isError && (
            <div className="mt-3 sm:mt-8 text-red-500 dark:text-light-grey text-lg flex gap-2 items-center justify-center">
              <Image
                src="/assets/images/icon-error.svg"
                alt="Error icon"
                width={iconSize}
                height={iconSize}
              />
              Please select an answer
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
