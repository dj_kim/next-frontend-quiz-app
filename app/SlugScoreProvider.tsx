'use client'

import { useParams } from 'next/navigation';
import { createContext, useEffect, useState } from 'react'
import { findQuizBySlug } from './utils';

export const SlugScoreContext = createContext({ slug: { title: "", icon: "" }, setSlug: (slug: { title: string; icon: string }) => { }, totalScore: 0, setTotalScore: (score: number) => { } })

export default function SlugScoreProvider({
  children,
}: {
  children: React.ReactNode
}) {
  const params = useParams<{ slug: string }>()

  const [slug, setSlug] = useState({ title: "", icon: "" })
  const [totalScore, setTotalScore] = useState(0)

  useEffect(() => {
    let quiz = findQuizBySlug(params.slug)
    if (quiz) {
      setSlug({ title: quiz.title, icon: quiz.icon })
    }
  }, [params.slug])

  return (
    <SlugScoreContext.Provider value={{ slug, setSlug, totalScore, setTotalScore }}>
      {children}
    </SlugScoreContext.Provider>
  )
}