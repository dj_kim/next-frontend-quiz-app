import { useContext, useState } from "react";
import { ThemeContext } from "../ThemeProvider";

export default function Toggle() {
  const { theme, setTheme } = useContext(ThemeContext)
  const [isChecked, setIsChecked] = useState(false)
  const handleToggle = () => {
    setIsChecked(!isChecked)
    setTheme(theme === 'light' ? 'dark' : 'light')
  }

  return (
    <div className="flex items-center justify-center">
      <input
        type="checkbox"
        id="toggle"
        className="hidden"
        checked={isChecked}
        onChange={handleToggle}
      />
      <label
        htmlFor="toggle"
        className="flex items-center cursor-pointer relative"
      >
        {/* toggle button */}
        <div className="w-6 h-3 bg-primary rounded-full shadow-inner ring-4 ring-primary"></div>
        {/* circle */}
        <div
          className={`dot absolute w-3 h-3 bg-white rounded-full shadow transition ${isChecked ? 'translate-x-full' : ''
            }`}
        ></div>
      </label>
    </div>
  )
}