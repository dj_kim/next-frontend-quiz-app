"use client"

import { useContext, useEffect, useState } from "react"
import { usePathname } from 'next/navigation'
import Link from "next/link"
import Image from "next/image"
import { useMediaQuery } from 'react-responsive'
import { ThemeContext } from "../ThemeProvider"
import TopicCard from "./TopicCard"
import Toggle from "./Toggle"

export default function Nav() {
  const pathname = usePathname()

  const { theme } = useContext(ThemeContext)
  const [themeIconSize, setThemeIconSize] = useState(14)

  const isMobile = useMediaQuery({ query: '(max-width: 640px)' })
  const isTablet = useMediaQuery({ query: '(min-width: 641px) and (max-width: 1024px)' })

  useEffect(() => {
    if (isMobile) {
      setThemeIconSize(14)
    } else if (isTablet) {
      setThemeIconSize(21)
    } else {
      setThemeIconSize(24)
    }
  }, [isMobile, isTablet])

  return (
    <nav className={`py-4 sm:pt-14 flex justify-between items-center`}>
      <div>
        {pathname.includes("quiz") && (
          <Link href="/">
            <TopicCard />
          </Link>
        )}
      </div>
      <div className="flex items-center gap-2 sm:gap-4">
        <Image src={`/assets/images/icon-sun-${theme === "light" ? "dark" : "light"}.svg`} alt="icon sun" width={themeIconSize} height={themeIconSize} loading="lazy" onError={(e) => console.error(e.target)} />
        <Toggle />
        <Image src={`/assets/images/icon-moon-${theme === "light" ? "dark" : "light"}.svg`} alt="icon moon" width={themeIconSize} height={themeIconSize} loading="lazy" onError={(e) => console.error(e.target)} />
      </div>
    </nav>
  )
}