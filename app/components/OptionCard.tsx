"use client"

import Image from "next/image"
import { useMediaQuery } from "react-responsive"

export default function OptionCard({ index, option, color, icon, outline, handleButtonClick }: { index: number, option: string, color: string, icon: string, outline: string, handleButtonClick: (option: string) => void }) {
  const isMobile = useMediaQuery({ query: '(max-width: 640px)' })
  let iconSize = isMobile ? 32 : 40
  return (
    <li key={index} className="mb-4">
      <button
        onClick={() => handleButtonClick(option)}
        className={`group select-none flex items-center gap-4 sm:gap-8 text-lg sm:text-[28px] font-medium p-3 lg:px-5 lg:py-[18px] rounded-xl sm:rounded-3xl w-full text-left bg-white dark:bg-navy dark:text-white ${outline && `outline outline-[3px] ${outline}`}`}
      >
        <div className={`flex-none w-10 h-10 sm:w-14 sm:h-14 ${color ? `${color} text-white` : "bg-light-grey"} flex justify-center items-center text-grey-navy font-medium rounded-lg group-hover:text-primary group-hover:bg-purple-100`}>
          {String.fromCharCode('A'.charCodeAt(0) + index)}
        </div>
        <div className="grow">
          {option}
        </div>
        <div className="flex-none">
          {icon && <Image src={`/assets/images/icon-${icon}.svg`} alt={`${icon} icon`} width={iconSize} height={iconSize} />}
        </div>
      </button>
    </li>
  )
}