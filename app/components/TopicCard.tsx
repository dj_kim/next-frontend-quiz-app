import Image from "next/image"
import { colorVariants } from "@/app/utils"
import { useContext } from "react"
import { SlugScoreContext } from "../SlugScoreProvider"
import { useMediaQuery } from "react-responsive"

export default function TopicCard() {
  const { slug } = useContext(SlugScoreContext)
  const isMobile = useMediaQuery({ query: '(max-width: 640px)' })
  let iconSize = isMobile ? 28.6 : 40

  return (
    <div className="flex gap-4 sm:gap-6 items-center">
      <div className={`flex-none w-10 h-10 ${colorVariants[slug.title as keyof typeof colorVariants]} sm:w-14 sm:h-14 flex justify-center items-center rounded-xl`}>
        <Image src={slug.icon} width={iconSize} height={iconSize} loading="lazy" alt={slug.icon} />
      </div>
      <div className="flex-1 text-lg sm:text-[28px] font-medium text-dark-navy dark:text-white">
        {slug.title}
      </div>
    </div>
  )
}