export default function ProgressBar({ progress, total }: { progress: number, total: number }) {
  return (
    <div className="h-4 bg-white dark:bg-navy rounded-xl flex items-center px-1">
      <div className="h-1/2 bg-primary rounded-xl" style={{ width: `${(progress / total) * 100}%` }}></div>
    </div>
  )
}