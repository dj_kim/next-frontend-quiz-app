"use client"

import Image from "next/image";
import { useMediaQuery } from "react-responsive";
import { colorVariants } from "../utils";

export default function Card({ icon, title }: { icon: string, title: string }) {
  const isMobile = useMediaQuery({ query: '(max-width: 640px)' })
  let iconSize = isMobile ? 28.6 : 40
  return (
    <div className="btn-option">
      <div className={`flex-none w-10 h-10 sm:w-14 sm:h-14 flex justify-center items-center rounded-xl ${colorVariants[title as keyof typeof colorVariants]}`}>
        <Image src={icon} width={iconSize} height={iconSize} alt={icon} />
      </div>
      <div className="flex-1 ml-4 sm:ml-8 text-lg sm:text-[28px] font-medium">
        {title}
      </div>
    </div>
  )
}