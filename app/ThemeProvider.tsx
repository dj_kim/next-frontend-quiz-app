"use client";

import { createContext, useState } from "react";

export const ThemeContext = createContext({
  theme: "light",
  setTheme: (theme: string) => {},
});

export default function ThemeProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [theme, setTheme] = useState("light");

  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      <div className={`${theme}`}>
        <div className="min-h-dvh theme bg-light-grey dark:bg-dark-navy">
          {children}
        </div>
      </div>
    </ThemeContext.Provider>
  );
}
