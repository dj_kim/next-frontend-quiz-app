import type { Metadata } from "next";
import { Rubik } from "next/font/google";
import "./globals.css";
import ThemeProvider from "./ThemeProvider";
import Nav from "./components/Nav";
import SlugScoreProvider from "./SlugScoreProvider";

const rubik = Rubik({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Frontend Quiz App",
  description: "Test your frontend knowledge with Frontend Quiz App",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={rubik.className}>
        <ThemeProvider>
          <SlugScoreProvider>
            <div className="px-6 sm:px-16 lg:px-[140px]">
              <Nav />
              {children}
            </div>
          </SlugScoreProvider>
        </ThemeProvider>
      </body>
    </html>
  );
}
