import type { Config } from "tailwindcss";

const config: Config = {
  darkMode: 'class',
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'desktop': "url('/assets/images/pattern-background-desktop-light.svg')",
        'tablet': "url('/assets/images/pattern-background-tablet-light.svg')",
        'mobile': "url('/assets/images/pattern-background-mobile-light.svg')",
        'desktop-dark': "url('/assets/images/pattern-background-desktop-dark.svg')",
        'tablet-dark': "url('/assets/images/pattern-background-tablet-dark.svg')",
        'mobile-dark': "url('/assets/images/pattern-background-mobile-dark.svg')",
      },
      colors: {
        primary: "#a729f5",
        secondary: "#26d782",
        warning: "#ee5454",
        "dark-navy": "#313e51",
        "navy": "#3b4d66",
        "grey-navy": "#626c7f",
        "light-bluish": "#abc1e1",
        "light-grey": "#f4f6fa",
        "light-orange": "#fff1e9",
        "light-green": "#e0fdef",
        "light-blue": "#ebf0ff",
        "light-purple": "#f6e7ff",
      },
      boxShadow: {
        'default': '0 16px 40px 0 rgba(143, 160, 193, 0.14)'
      }
    },
  },
  plugins: [],
};
export default config;
